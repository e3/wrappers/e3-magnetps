import pytest
from epics import PV, caget, caput, ca
from run_iocsh import IOC
from os import environ
from pathlib import Path
from time import sleep


def start_ioc(cmd_file, curve):
    ca.initialize_libca()

    env_vars = ["EPICS_BASE", "E3_REQUIRE_VERSION", "TEMP_CELL_PATH"]
    base, require, cell_path = map(environ.get, env_vars)
    if not cell_path:
        cell_path = Path(__file__).parent.parent / "cellMods"
    else:
        cell_path = Path(cell_path)
    assert cell_path.is_dir()

    assert base
    assert require

    iocsh_path = Path(base) / "require" / require / "bin" / "iocsh"
    assert iocsh_path.is_file()

    test_cmd = Path(__file__).parent / cmd_file
    assert test_cmd.is_file()

    args = ["-l", str(cell_path), str(test_cmd), f"CURVE={curve}"]
    ioc = IOC(*args, ioc_executable=iocsh_path, timeout=20.0)
    ioc.start()
    assert ioc.is_running()
    sleep(1)
    return ioc


@pytest.fixture
def testioc1(curve):
    ioc = start_ioc("st_test1.cmd", curve)
    yield ioc
    ioc.exit()
    sleep(1)
    assert not ioc.is_running()
    sleep(1)
    ca.flush_io()
    ca.clear_cache()


class TestUnitTests:
    @pytest.mark.parametrize(
        "curve, current, expectedFld, tolerance",
        [
            ("LEBT-010_BMD-CH-01", -30, -0.00255, 0.001),
            ("LEBT-010_BMD-CH-02", 15, 0.001275, 0.001),
            ("LEBT-010_BMD-CV-01", 30, 0.0021249, 0.001),
            ("LEBT-010_BMD-CV-02", 10, 0.0007083, 0.001),
            ("LEBT-010_BMD-Sol-01", 250, 0.207275, 0.001),
            ("LEBT-010_BMD-Sol-02", 375, 0.310912, 0.001),
            ("MEBT-010_BMD-CH-001", -4.25, -0.0237483, 0.001),
            ("MEBT-010_BMD-CH-002", 9.1, 0.0508495, 0.001),
            ("MEBT-010_BMD-CH-003", 0, 0, 0.001),
            ("MEBT-010_BMD-CH-004", 20, 0.1117, 0.001),
            ("MEBT-010_BMD-CH-005", -20, -0.1117, 0.001),
            ("MEBT-010_BMD-CH-006", 8.5, 0.04749, 0.001),
            ("MEBT-010_BMD-CH-006", 4.25, 0.02374, 0.001),
            ("MEBT-010_BMD-CH-007", -9.1, -0.05084, 0.001),
            ("MEBT-010_BMD-CH-008", 9001, 50.2963, 0.001),
            ("MEBT-010_BMD-CH-009", 8.1, 0.04526, 0.001),
            ("MEBT-010_BMD-CH-010", 3, 0.01676, 0.001),
            ("MEBT-010_BMD-CH-011", -3, -0.01676, 0.001),
            ("MEBT-010_BMD-CV-001", 8.5, -0.04749, 0.001),
            ("MEBT-010_BMD-CV-002", -8.5, 0.04749, 0.001),
            ("MEBT-010_BMD-CV-003", 8.5, -0.04749, 0.001),
            ("MEBT-010_BMD-CV-004", -8.5, 0.04749, 0.001),
            ("MEBT-010_BMD-CV-005", 8.5, -0.04749, 0.001),
            ("MEBT-010_BMD-CV-006", -8.5, 0.04749, 0.001),
            ("MEBT-010_BMD-CV-007", 8.5, -0.04749, 0.001),
            ("MEBT-010_BMD-CV-008", -8.5, 0.04749, 0.001),
            ("MEBT-010_BMD-CV-009", 8.5, -0.04749, 0.001),
            ("MEBT-010_BMD-CV-010", -8.5, 0.04749, 0.001),
            ("MEBT-010_BMD-CV-011", 8.5, -0.04749, 0.001),
            ("MEBT-010_BMD-QH-002", -220, -34.69837, 0.001),
            ("MEBT-010_BMD-QH-004", 120, 20.01519, 0.001),
            ("MEBT-010_BMD-QH-005", 250, 37.41798, 0.001),
            ("MEBT-010_BMD-QH-007", -20, -3.4656, 0.001),
            ("MEBT-010_BMD-QH-009", 30, 5.1369, 0.001),
            ("MEBT-010_BMD-QH-011", 110, 18.4004, 0.001),
            ("MEBT-010_BMD-QV-001", 90, -15.1322, 0.001),
            ("MEBT-010_BMD-QV-003", -90, 15.0258, 0.001),
            ("MEBT-010_BMD-QV-006", 90, -15.1039, 0.001),
            ("MEBT-010_BMD-QV-008", -90, 15.1462, 0.001),
            ("MEBT-010_BMD-QV-010", 90, -15.0978, 0.001),
        ],
    )
    def test_conversions(self, testioc1, curve, current, expectedFld, tolerance):
        ## Set the current setpoint to zero
        caput("testP:testR:CurTest", 0)
        sleep(1)

        ## Verify that field read back is zero
        assert caget("testP:testR:Fld-RB", use_monitor=False) == pytest.approx(
            0, tolerance
        )

        ## Set the current setpoint
        caput("testP:testR:CurTest", current)
        sleep(1)

        ## Verify that field read back is updated accordingly
        fld = caget("testP:testR:Fld-RB", use_monitor=False)
        assert fld == pytest.approx(expectedFld, tolerance)
