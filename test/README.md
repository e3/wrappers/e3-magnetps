# Test - e3-magnetps
This directory contains the sources for the automatic testing of the e3-magnetps module.

## Prerequisites
In order to run the test suite, you must install the following:

 * python3

On CentOS 7, run the following:

```
sudo yum install -y python3
```

And the following python modules:

 * pytest
 * pyepics
 * run-iocsh

You can use the following pip3 commands:

```
pip3 install pytest pyepics
pip3 install run-iocsh -i https://artifactory.esss.lu.se/artifactory/api/pypi/pypi-virtual/simple
```

You must configure the EPICS environment before running the test suite.
For the E3 environment, this requires you to ``source setE3Env.bash``.


## Running the test suite
The primary way to run the is to run the following from the e3-magnetps root:
```
make test
```

You can also run the test suite from this folder using the following command, assuming you have installed in cell mode (make cellinstall):
```
pytest -v test_script.py
```

## Expected result
A successful run should look something like this:
```
======================================================= test session starts ========================================================
platform linux -- Python 3.6.8, pytest-6.2.5, py-1.10.0, pluggy-1.0.0 -- /usr/bin/python3
cachedir: .pytest_cache
rootdir: /home/karlvestin/e3
collected 1 item

test_script.py::TestUnitTests::test_1 PASSED                                                                                 [100%]

======================================================== 1 passed in 4.29s =========================================================
```
