require magnetps

# Load a simple PV to test current
dbLoadRecords("$(E3_CMD_TOP)/magnetps-test.template", "P=testP:, R=testR:")

# Load magnet PS module
iocshLoad("$(magnetps_DIR)/magnetps.iocsh", "P=testP:, R=testR:, SETCURPV=CurTest, READCURPV=CurTest, CONVERSIONTABLE=$(CURVE), EGU=T/m, ADEL=0")

## Run IOC
iocInit()
